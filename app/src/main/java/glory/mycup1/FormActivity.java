package glory.mycup1;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.system.Os;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FormActivity extends AppCompatActivity implements View.OnClickListener {

    EditText txtName;
    EditText txtFamily;
    EditText txtEmail;
    EditText txtMobile;
    EditText txtPassword;
    Button btnRegister;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        bindView();
        btnRegister.setOnClickListener(this);
    }

    public void bindView() {
        txtName = findViewById(R.id.txtName);
        txtFamily = findViewById(R.id.txtFamily);
        txtEmail = findViewById(R.id.txtEmail);
        txtMobile = findViewById(R.id.txtMobile);
        txtPassword = findViewById(R.id.txtPassword);
        btnRegister = findViewById(R.id.btnRegister);
        tvResult = findViewById(R.id.tvResult);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnRegister) {
            registerResult();
        }
    }

    @SuppressLint("SetTextI18n")
    public void registerResult() {
        String name = txtName.getText().toString();
        String family = txtFamily.getText().toString();
        String email = txtEmail.getText().toString();
        String mobile = txtMobile.getText().toString();
        String password = txtPassword.getText().toString();
        Boolean result = false;
        if (name.trim().matches("")) {
            txtName.setText("");
            tvResult.setText(getString(R.string.please_enter_name));
        } else if (family.trim().matches("")) {
            txtFamily.setText("");
            tvResult.setText(getString(R.string.please_enter_family));
        } else if (email.trim().matches("")) {
            txtEmail.setText("");
            tvResult.setText(getString(R.string.please_enter_email));
        } else if (mobile.trim().matches("")) {
            tvResult.setText(getString(R.string.please_enter_mobile));
        } else if (password.trim().matches("")) {
            txtPassword.setText("");
            tvResult.setText(getString(R.string.please_enter_password));
        } else {
            //tvResult.setText("Dear " + name + " " + family + ".. welcome!");
            tvResult.setText(name + " " + family + "\n" + getString(R.string.welcomeMessage));
            txtName.setText("");
            txtFamily.setText("");
            txtEmail.setText("");
            txtMobile.setText("");
            txtPassword.setText("");
            result = true;
        }
        if (result) {
            Toast.makeText(this, getString(R.string.successMessage), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.errorMessage), Toast.LENGTH_SHORT).show();
        }
    }
}
